// App config the for production environment.
// Do not require this directly. Use ./src/config instead.


export default {
  apiBaseUrl: "http://api.databrew.org",
}
