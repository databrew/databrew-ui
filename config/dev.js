// App config the for development environment.
// Do not require this directly. Use ./src/config instead.


const WEBPACK_HOST = process.env.HOST || "localhost"


export default {
  apiBaseUrl: `http://${WEBPACK_HOST}:3000`,
}
