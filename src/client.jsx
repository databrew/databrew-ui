import "babel/polyfill"

import {Provider} from "react-redux"
import {Router} from "react-router"
import createBrowserHistory from "history/lib/createBrowserHistory"
import React from "react"

import configureStore from "./shared/store"
import routes from "./shared/routes"

import "rebass/rebass.css"


const history = createBrowserHistory()
const store = configureStore(window.__INITIAL_STATE__)


React.render(
  <Provider store={store}>
    {() => <Router children={routes} history={history} />}
  </Provider>,
  document.getElementById("app-mount-node"),
)
