import {applyMiddleware, createStore} from "redux"
import thunk from "redux-thunk"

import promiseMiddleware from "./middlewares/promise"
import reducer from "./reducers"


const createStoreWithMiddleware = applyMiddleware(
  thunk,
  promiseMiddleware,
)(createStore)


// Creates a preconfigured store for this application.
export default function configureStore(initialState) {
  return createStoreWithMiddleware(reducer, initialState)
}
