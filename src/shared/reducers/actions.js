import {LIST_ACTIONS} from "../constants"


export default function actionsReducer(state = [], action) {
  switch (action.type) {
  case LIST_ACTIONS:
    return [...action.result]
  default:
    return state
  }
}
