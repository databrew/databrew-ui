import {LIST_DATASETS} from "../constants"


export default function datasetsReducer(state = [], action) {
  switch (action.type) {
  case LIST_DATASETS:
    return [...action.result]
  default:
    return state
  }
}
