import {combineReducers} from "redux"

import actionsReducer from "./actions"
import datasetsReducer from "./datasets"


// Updates the data for different actions.
export default combineReducers({
  actions: actionsReducer,
  datasets: datasetsReducer,
})
