import {IndexRoute, Route} from "react-router"

import About from "../shared/components/about"
import Actions from "../shared/components/actions"
import App from "../shared/components/app"
import Datasets from "../shared/components/datasets"
import Home from "../shared/components/home"
import NotFound from "../shared/components/not-found"


export default (
  <Route component={App} path="/">
    <IndexRoute component={Home} />
    <Route component={About} path="about" />
    <Route component={Actions} path="actions" />
    <Route component={Datasets} path="datasets" />
    <Route component={NotFound} isNotFound path="*" />
  </Route>
)
// <Route component={Dataset} path="datasets/:urlPath" />
