import {fetchApiJson} from "../fetchers"
import {LIST_DATASETS} from "../constants"
import schemas from "../schemas"


export function listDatasets() {
  return {
    type: LIST_DATASETS,
    promise: fetchApiJson("datasets", schemas.DATASET_ARRAY),
  }
}


export function listDatasetsIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchDatasets(getState())) {
      return dispatch(listDatasets())
    }
  }
}


function shouldFetchDatasets(state) {
  const {datasets} = state
  return !datasets || !datasets.length
}
