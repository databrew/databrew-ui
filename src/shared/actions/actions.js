import {fetchApiJson} from "../fetchers"
import {LIST_ACTIONS} from "../constants"
import schemas from "../schemas"


export function listActions() {
  return {
    type: LIST_ACTIONS,
    promise: fetchApiJson("actions", schemas.ACTION_ARRAY),
  }
}


export function listActionsIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchActions(getState())) {
      return dispatch(listActions())
    }
  }
}


function shouldFetchActions(state) {
  const {actions} = state
  return !actions || !actions.length
}
