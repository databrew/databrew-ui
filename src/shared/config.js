// Load different configs for production or development

const configFile = process.env.NODE_ENV === "production" ? "prod.js" : "dev.js"

const config = require("../../config/" + configFile)


export default config
