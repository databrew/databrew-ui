import "isomorphic-fetch"
// import {normalize} from "normalizr"
import url from "url"

import config from "./config"


// fetch helpers

function checkStatus(response) {
  if (response.ok) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

function parseJSON(response) {
  return response.json()
}


// fetchers

export function fetchApiJson(path, schema, fetchOptions) {
  // return fetch(url.resolve(config.apiBaseUrl, path), {method: "post"})
  const fetchUrl = url.resolve(config.apiBaseUrl, path)
  console.log("Fetching " + fetchUrl)
  return fetch(fetchUrl, fetchOptions)
    .then(checkStatus)
    .then(parseJSON)
    .then((json) => json.data)
    // normalize(json, schema)
}


// Fetch data needed to render the view corresponding to a given route.
export function fetchRouteData(renderProps, store) {
  const {components} = renderProps
  const promises = components.reduce((memo, component) => {
    const fetchData = component.WrappedComponent ? // Is it a Redux "connected" component?
      component.WrappedComponent.fetchData :
      component.fetchData
    if (fetchData) {
      memo.push(fetchData(renderProps, store))
    }
    return memo
  }, [])
  return Promise.all(promises)
}
