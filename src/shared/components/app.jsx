import {Component, PropTypes} from "react"
import {Container, Flex, Toolbar} from "rebass"

import NavItemLink from "./navitemlink"


export default class App extends Component {
  static propTypes = {
    children: PropTypes.node,
  }
  render() {
    const {children} = this.props
    return (
      <div>
        <Toolbar color="blue">
          <Flex>
            Databrew
          </Flex>
          <NavItemLink to="/">Home</NavItemLink>
          <NavItemLink to="/about">About</NavItemLink>
          <NavItemLink to="/actions">Actions</NavItemLink>
          <NavItemLink to="/datasets">Datasets</NavItemLink>
        </Toolbar>
        <Container>
          {children}
        </Container>
      </div>
    )
  }
}
