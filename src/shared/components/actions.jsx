// import {bindActionCreators} from "redux"
import {Component, PropTypes} from "react"
import {connect} from "react-redux"

import * as actionsActions from "../actions/actions"


@connect(
  state => ({actions: state.actions}),
  dispatch => ({
    onListActions: () => dispatch(actionsActions.listActions()),
    onListActionsIfNeeded: () => dispatch(actionsActions.listActionsIfNeeded()),
  }),
)
export default class Actions extends Component {
  static fetchData = function(renderProps, store) {
    return store.dispatch(actionsActions.listActions())
  }
  static propTypes = {
    actions: PropTypes.arrayOf(PropTypes.object),
    onListActions: PropTypes.func.isRequired,
    onListActionsIfNeeded: PropTypes.func.isRequired,
  }
  componentWillMount() {
    this.props.onListActionsIfNeeded()
  }
  render() {
    const {actions, onListActions} = this.props
    return (
      <section>
        <p>
          Actions
        </p>
        <button onClick={() => onListActions()}>Reload</button>
        <pre>{JSON.stringify(actions, null, 2)}</pre>
      </section>
    )
  }
}
