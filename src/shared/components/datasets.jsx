// import {bindActionCreators} from "redux"
import {Component, PropTypes} from "react"
import {connect} from "react-redux"

import * as datasetsActions from "../actions/datasets"


@connect(
  state => ({datasets: state.datasets}),
  dispatch => ({
    onListDatasets: () => dispatch(datasetsActions.listDatasets()),
    onListDatasetsIfNeeded: () => dispatch(datasetsActions.listDatasetsIfNeeded()),
  }),
)
export default class Datasets extends Component {
  static fetchData = function(renderProps, store) {
    return store.dispatch(datasetsActions.listDatasets())
  }
  static propTypes = {
    datasets: PropTypes.arrayOf(PropTypes.object),
    onListDatasets: PropTypes.func.isRequired,
    onListDatasetsIfNeeded: PropTypes.func.isRequired,
  }
  componentWillMount() {
    this.props.onListDatasetsIfNeeded()
  }
  render() {
    const {datasets, onListDatasets} = this.props
    return (
      <section>
        <p>
          Datasets
        </p>
        <button onClick={() => onListDatasets()}>Reload</button>
        <pre>{JSON.stringify(datasets, null, 2)}</pre>
      </section>
    )
  }
}
