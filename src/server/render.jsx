import {Provider} from "react-redux"
import {renderToStaticMarkup, renderToString} from "react"
import {RoutingContext, match} from "react-router"
import createLocation from "history/lib/createLocation"

import {fetchRouteData} from "../shared/fetchers"
import configureStore from "../shared/store"
import HtmlDocument from "./html-document"
import routes from "../shared/routes"


export default function handleRender(req, res, next) {
  const store = configureStore()
  const location = createLocation(req.url)
  match({routes, location}, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      res.redirect(301, redirectLocation.pathname + redirectLocation.search)
    } else if (error) {
      // TODO Disable in production?
      next(error)
    } else if (renderProps == null) {
      res.send(404, "Not found")
    } else {
      fetchRouteData(renderProps, store)
        .then(() => res.send(renderHtmlDocument(renderProps, store)))
        .catch((error1) => next(error1))
    }
  })
}


function loadWebpackAssets() {
  const webpackAssetsFilePath = "../../webpack-assets.json"
  let webpackAssets
  if (process.env.NODE_ENV === "production") {
    webpackAssets = require(webpackAssetsFilePath)
  } else if (process.env.NODE_ENV === "development") {
    webpackAssets = require(webpackAssetsFilePath)
    // Do not cache webpack stats: the script file would change since
    // hot module replacement is enabled in the development env
    delete require.cache[require.resolve(webpackAssetsFilePath)]
  }
  return webpackAssets
}


function renderHtmlDocument(renderProps, store) {
  const appHtml = renderToString(
    <Provider store={store}>
      {() => <RoutingContext {...renderProps} />}
    </Provider>
  )
  const webpackAssets = loadWebpackAssets()
  const html = renderToStaticMarkup(
    <HtmlDocument
      appHtml={appHtml}
      appInitialState={store.getState()}
      cssUrls={webpackAssets.main.css}
      jsUrls={webpackAssets.main.js}
    />
  )
  const doctype = "<!DOCTYPE html>"
  return doctype + html
}
