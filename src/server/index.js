import path from "path"

import express from "express"
import favicon from "serve-favicon"

import handleRender from "./render"


const app = express()
app.use(favicon(path.resolve(__dirname, "../../public/favicon.ico")))
app.use(handleRender)

const PORT = process.env.PORT
if (!PORT) {
  console.error("PORT environment variable must be defined")
}

app.listen(PORT, () => {
  console.log(`Server listening at http://localhost:${PORT}/`)
})
