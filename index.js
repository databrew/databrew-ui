require("babel/register")({
  stage: 0,
})

// Provide React global variable because of babel JSX transformation which generates React.createElement.
global.React = require("react")

if (process.env.NODE_ENV === "production" || require("piping")({
  hook: true,
  ignore: /(\/\.|~$|webpack-assets.json)/,
})) {
  require("./src/server/index.js")
}
