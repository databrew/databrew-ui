// This is the webpack config to use during development.
// It enables the hot module replacement, the source maps and inline CSS styles.

import ErrorNotificationPlugin from "webpack-error-notification"
import ExtractTextPlugin from "extract-text-webpack-plugin"
import path from "path"
import webpack from "webpack"

import writeAssets from "./src/server/write-assets"


const assetsPath = path.resolve(__dirname, "public")
const WEBPACK_HOST = process.env.WEBPACK_HOST || "localhost"
const WEBPACK_PORT = parseInt(process.env.WEBPACK_PORT)


export default {
  // devtool: "eval", // Transformed code
  devtool: "source-map", // Original code
  entry: {
    "main": [
      `webpack-dev-server/client?http://${WEBPACK_HOST}:${WEBPACK_PORT}`,
      "webpack/hot/only-dev-server",
      "./src/client.jsx",
    ],
  },
  output: {
    path: assetsPath,
    filename: "[name]-bundle-[hash].js",
    publicPath: `http://${WEBPACK_HOST}:${WEBPACK_PORT}/`,
  },
  module: {
    loaders: [
      {
        loader: ExtractTextPlugin.extract("css-loader!cssnext-loader"),
        test: /\.css$/,
      },
      {
        exclude: /node_modules/,
        loaders: ["react-hot", "babel?stage=0"],
        test: /\.(js|jsx)$/,
      },
    ],
  },
  resolve: {
    extensions: ["", ".js", ".jsx"],
  },
  progress: true,
  plugins: [
    // hot reload
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),

    // print a webpack progress
    new webpack.ProgressPlugin((percentage) => {
      if (percentage === 1) {
        process.stdout.write("Bundle is ready")
      }
    }),

    new ErrorNotificationPlugin(process.platform === "linux" && function(msg) {
      if (!this.lastBuildSucceeded) {
        require("child_process").exec("notify-send --hint=int:transient:1 Webpack " + msg)
      }
    }),

    new webpack.DefinePlugin({
      "process.env": {
        BROWSER: JSON.stringify(true),
        HOST: JSON.stringify(process.env.HOST),
        NODE_ENV: JSON.stringify("development"),
      },
    }),

    new webpack.ProvidePlugin({
      React: "react", // For babel JSX transformation which generates React.createElement.
    }),

    new ExtractTextPlugin("rebass-[contenthash].css"),

    function() { this.plugin("done", writeAssets(path.resolve(__dirname, "webpack-assets.json"))) },
  ],
}
